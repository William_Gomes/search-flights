import Foundation

struct ModelMock: Codable, Equatable {
    
    var key: String = ""
    var value: String = ""
    
    enum CodingKeys: String, CodingKey {
        
        case key = "key"
        case value = "value"
    }

    init(key: String, value: String) {

         self.key = key
         self.value = value
     }
}
