import Foundation

@testable import search_flights

final class ParserMock: ParserProtocol {

    let showFailure: Bool

    init(showFailure: Bool = false ) {

        self.showFailure = showFailure
    }

    func parseData(data: Data) -> Result<[ModelMock]?, NetworkingError<String>> {
        
        if showFailure {

            return .failure(.unableToParseData)

        } else {

            if data.isEmpty {

                return .success(nil)

            } else {

                 return .success([ModelMock(key: "", value: "")])
            }
        }
    }

    func parseError(statusCode: Int, data: Data) -> NetworkingError<String> {

        guard let httpStatusCode = HTTPStatusCode(rawValue: statusCode) else {

            return NetworkingError.invalidHTTPStatusCode(code: statusCode)
        }

        return NetworkingError.httpStatusCode(httpError: httpStatusCode)
    }
}
