import XCTest

@testable import search_flights

class JSONParserTests: XCTestCase {

    func testParseData_WithEmptyData_ShouldReturnEmptyModel() {
        
        //Given
        let jsonParser = JSONParser<ModelMock>()
        
        //When
        let result = jsonParser.parseData(data: Data())
        
        //Then
        if case let Result.success(responseModel) = result {
            
            XCTAssertEqual(responseModel, nil)
            
        } else {
            
            XCTFail("Result is success when is supposed to be failure")
        }
    }
    
    func testParseData_WithInvalidData_ShouldReturnUnableToParseData() {
        
        //Given
        let jsonParser = JSONParser<ModelMock>()
        
        //When
        let result = jsonParser.parseData(data: Data("someString".utf8))

        //Then
        if case let Result.failure(error) = result {
            
            XCTAssertTrue(error == NetworkingError.unableToParseData)
            
        } else {
            
            XCTFail("Result is success when is supposed to be failure")
        }
    }

    func testParserError_WithInvalidStatusCode_ShouldReturnInvalidHTTPStatusCode() {
        
        //Given
        let jsonParser = JSONParser<ModelMock>()

        //When
        let error = jsonParser.parseError(statusCode: 99, data: Data())
        
        //Then
        XCTAssertTrue(error == NetworkingError.invalidHTTPStatusCode(code: 99))
    }
    
    func testParserError_WithValidStatusCode_ShouldReturnHTTPStatusCode() {
        
        //Given
        let jsonParser = JSONParser<ModelMock>()
        
        //When
        let error = jsonParser.parseError(statusCode: 404, data: Data())
        
        //Then
        if case let NetworkingError.httpStatusCode(httStatusCode) = error {
            
            XCTAssertTrue(httStatusCode == HTTPStatusCode.NotFound)

        } else {

            XCTFail("Invalid HTTP Status Code")
        }
    }
}
