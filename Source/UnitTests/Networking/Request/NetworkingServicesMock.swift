import Alamofire
import Foundation

@testable import search_flights

final class NetworkingServicesMock: NetworkingServicesProtocol {
    
    var basePath: String
    var path: String
    var method: HTTPMethod
    var encoding: ParameterEncoding
    var parameters: Parameters?
    var headers: HTTPHeaders?
    
    init(basePath: String,
         path: String,
         method: HTTPMethod = .get,
         encoding: ParameterEncoding = JSONEncoding(),
         parameters: Parameters? = nil,
         headers: HTTPHeaders? = nil) {
        
        self.basePath = basePath
        self.path = path
        self.method = method
        self.encoding = encoding
        self.parameters = parameters
        self.headers = headers
    }
}
