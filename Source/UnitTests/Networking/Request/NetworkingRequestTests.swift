import Alamofire
import Foundation
import Mockingjay
import XCTest

@testable import search_flights

final class NetworkingRequestTests: XCTestCase {
    
    private let networkingRequest = NetworkingRequest(session: .default)

    private func buildNetworkingRequestError(with error: Error) -> NetworkingError<String> {

        let responseError: NetworkingError<String> = networkingRequest.exposedTranslateResponseError(error)
        
        return responseError
    }
    
    private func buildDataResponseWith(_ result: AFResult<Data>,
                                       statusCode: Int? = 404,
                                       data: Data? = Data()) -> AFDataResponse<Data> {
        
        let url = URL(string: "www.google.com")!
        
        var httpURLResponse: HTTPURLResponse?
        
        if let statusCode = statusCode {
            httpURLResponse = HTTPURLResponse(url: url,
                                              statusCode: statusCode,
                                              httpVersion: nil,
                                              headerFields: nil)
        }
        
        let urlRequest = URLRequest(url: url)
        
        return AFDataResponse(request: urlRequest,
                              response: httpURLResponse,
                              data: data,
                              metrics: nil,
                              serializationDuration: 0,
                              result: result)
    }
    
    func testRequest_WithValidResponse_ShouldParseData() {
        
        //Given
        let exp = expectation(description: "Expect to load closure")
        let body = ["user": "Kyle"]
        stub(everything, json(body))

        //When
        networkingRequest.request(NetworkingServicesSearchFlights.stations,
                                  parser: ParserMock(),
                                  onSuccess: { responseModel in
                                    
                                    //Then
                                    exp.fulfill()
                                    XCTAssertNotEqual(responseModel, nil)
        }, onError: { _ in
            
            exp.fulfill()
            XCTFail("It's not supposed to be an Error")
        })
        
        waitForExpectations(timeout: 3)
    }
    
    func testRequest_WithInvalidHTTPResponse_ShouldReturnErrorNotFound() {
        
        //Given
        let exp = expectation(description: "Expect to load closure")
        stub(everything, http(999))
        
        //When
        networkingRequest.request(NetworkingServicesSearchFlights.stations,
                                  parser: ParserMock(),
                                  onSuccess: { _ in
                                    
                                    exp.fulfill()
                                    XCTFail("It's not supposed to be an Success")

        }, onError: { responseError in
            
            //Then
            exp.fulfill()
            XCTAssertEqual(responseError, NetworkingError.invalidHTTPStatusCode(code: 999))
        })
        
        waitForExpectations(timeout: 3)
    }
    
    func testRequest_WithDealocatingNetworkingRequest_ShouldReturnSelfDeallocated() {
        
        //Given
        var request: NetworkingRequest? = NetworkingRequest(session: .default)
        let exp = expectation(description: "Expect to load closure")
        
        //When
        request?.request(NetworkingServicesSearchFlights.stations,
                         parser: ParserMock(),
                         onSuccess: { _ in
                            
                            exp.fulfill()
                            XCTFail("It's not supposed to be an Success")
                            
        }, onError: { responseError in
            
            //Then
            exp.fulfill()
            XCTAssertEqual(responseError, NetworkingError.selfDeallocated)
        })
        
        request = nil
        waitForExpectations(timeout: 3)
    }

    func testParseResponse_WithInvalidURLError_ShouldReturnInvalidURL() {
        
        //Given
        let error = AFError.invalidURL(url: "url")
        let dataResponse = buildDataResponseWith(Result.failure(error), statusCode: nil, data: nil)
        let exp = expectation(description: "Expect to load closure")
    
        //When
        networkingRequest.exposedParseResponse(response: dataResponse,
                                               parser: ParserMock(),
                                               onSuccess: { _ in
                                                
                                                exp.fulfill()
                                                XCTFail("It's not supposed to success")
        }, onError: { responseError in
            
            //Then
            exp.fulfill()
            XCTAssertEqual(responseError, NetworkingError<String>.invalidURL)
        })
        
        waitForExpectations(timeout: 3)
    }
    
    func testParseResponse_WithEmptyData_ShouldReturnNil() {
        
        //Given
        let dataResponse = buildDataResponseWith(Result.success(Data()))
        let exp = expectation(description: "Expect to load closure")

        //When
        networkingRequest.exposedParseResponse(response: dataResponse,
                                               parser: ParserMock(),
                                               onSuccess: { responseModel in
                                                
                                                //Then
                                                exp.fulfill()
                                                XCTAssertNil(responseModel)
                                                
        }, onError: { _ in
            
            exp.fulfill()
            XCTFail("It's not supposed to failure")
        })
        
        waitForExpectations(timeout: 3)
    }
    
    func testParseResponse_WithParserError_ShouldReturnParserError() {
        
        //Given
        let exp = expectation(description: "Expect to load closure")
        let dataResponse = buildDataResponseWith(Result.success(Data()))
        
        //When
        networkingRequest.exposedParseResponse(response: dataResponse,
                                               parser: ParserMock(showFailure: true),
                                               onSuccess: { _ in
                                                
                                                exp.fulfill()
                                                XCTFail("It's not supposed to success")
                                                
        }, onError: { responseError in
            
            //Then
            exp.fulfill()
            XCTAssertEqual(responseError, NetworkingError<String>.unableToParseData)
        })
        
        waitForExpectations(timeout: 3)
    }
    
    func testParseResponse_WithNoInternet_ShouldReturnNoInternetConnection() {
        
        //Given
        let exp = expectation(description: "Expect to load closure")
        let error = NSError(domain: "", code: -1009, userInfo: nil)
        let afError = AFError.sessionTaskFailed(error: error)
        let dataResponse = buildDataResponseWith(Result.failure(afError), statusCode: nil, data: nil)
        
        //When
        networkingRequest.exposedParseResponse(response: dataResponse,
                                               parser: ParserMock(showFailure: true),
                                               onSuccess: { _ in
                                                
                                                exp.fulfill()
                                                XCTFail("It's not supposed to success")
                                                
        }, onError: { responseError in
            
            //Then
            exp.fulfill()
            XCTAssertEqual(responseError, NetworkingError<String>.noInternetConnection)
        })
        
        waitForExpectations(timeout: 3)
    }
    
    func testVerifyResponse_WithStatusCodeAndData_ShouldReturnNetworkingError() {
        
        //Given
        let exp = expectation(description: "Expect to load closure")
        let dataResponse = buildDataResponseWith(Result.failure(.explicitlyCancelled))
        let error = NSError(domain: "", code: 999, userInfo: nil)
        
        //When
        networkingRequest.exposedVerifyResponseError(response: dataResponse,
                                                     error: error,
                                                     parser: ParserMock(),
                                                     onError: { error  in
                                                        
                                                        //Then
                                                        let notFound: NetworkingError<String>
                                                        notFound = .httpStatusCode(httpError: .NotFound)
                                                        XCTAssertEqual(notFound, error)
                                                        exp.fulfill()
        })
        
        waitForExpectations(timeout: 3)
    }
    
    func testVerifyResponse_WithStatusCodeAndNoData_ShouldReturnNetworkingError() {
    
        //Given
        let exp = expectation(description: "Expect to load closure")
        let dataResponse = buildDataResponseWith(Result.failure(.explicitlyCancelled), data: nil)
        let error = NSError(domain: "", code: -1009, userInfo: nil)
        
        //When
        networkingRequest.exposedVerifyResponseError(response: dataResponse,
                                                     error: error,
                                                     parser: ParserMock(),
                                                     onError: { error  in
                                                        
                                                        //Then
                                                        let notFound: NetworkingError<String>
                                                        notFound = .httpStatusCode(httpError: .NotFound)
                                                        XCTAssertEqual(notFound, error)
                                                        exp.fulfill()
        })
        
        waitForExpectations(timeout: 3)
    }
    
    func testTranslateError_WithValidErrorCode_ShouldReturnNoInternetConnection() {

        //Given
        let error = NSError(domain: "", code: -1009, userInfo: nil)

        //When
        let responseError = buildNetworkingRequestError(with: AFError.sessionTaskFailed(error: error))

        //Then
        XCTAssertTrue(responseError == NetworkingError.noInternetConnection)
    }

    func testTranslateError_WithValidUrlError_ShouldReturnInvalidURL() {

        //Given
        let afError = AFError.invalidURL(url: "wwwhh.com")

        //When
        let responseError = buildNetworkingRequestError(with: afError)

        //Then
        XCTAssertTrue(responseError == NetworkingError.invalidURL)
    }

    func testTranslateError_WithInvalidErrorCode_ShouldReturnNotDefined() {

        //Given
        let afError = AFError.sessionDeinitialized

        //When
        let responseError = buildNetworkingRequestError(with: afError)

        //Then
        if case let NetworkingError.notDefined(code, _) = responseError {

            XCTAssertEqual(16, code)

        } else {

            XCTFail("Invalid NetworkingError")
        }
    }
    
    func testTranslateError_WithInvalidError_ShouldReturnNotDefined() {

           //Given
           let error = NSError(domain: "", code: 999, userInfo: nil)

           //When
           let responseError = buildNetworkingRequestError(with: error)

           //Then
           if case let NetworkingError.notDefined(code, _) = responseError {

               XCTAssertEqual(999, code)

           } else {

               XCTFail("Invalid NetworkingError")
           }
       }
}
