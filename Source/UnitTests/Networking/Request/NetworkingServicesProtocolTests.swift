import Alamofire
import Foundation
import XCTest

@testable import search_flights

final class NetworkingServicesProtocolTests: XCTestCase {
    
    func testAsURLRequest_WithEmptyURL_ShouldReturnInvalidURL() {
    
        //Given
        let networkingServicesMock = NetworkingServicesMock(basePath: "", path: "")
        
        do {
            
            //When
            _ = try networkingServicesMock.asURLRequest()
            
        } catch {
            
            //Then
            
            if let networkingError = error as? NetworkingError<String> {
                
                XCTAssertEqual(networkingError, NetworkingError.invalidURL)
    
            } else {
                
                XCTFail("Invalid NetworkingError")
            }
        }
    }
    
    func testAsURLRequest_WithValidURL_ShouldReturnURLRequest() {
        
        //Given
        let url = "www.google.com"
        let networkingServicesMock = NetworkingServicesMock(basePath: url, path: "")
        
        do {
            
            //When
            let urlRequest = try networkingServicesMock.asURLRequest()
            XCTAssertEqual(urlRequest.url?.absoluteString, url)

        } catch {
            
            //Then
            XCTFail("Returned NetworkingError")
        }
    }
}
