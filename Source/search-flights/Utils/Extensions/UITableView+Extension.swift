import UIKit

extension UITableViewCell: ReusableView { }
extension UITableViewHeaderFooterView: ReusableView { }

extension UITableView {

    public func registerNib<T: UITableViewCell>(for cellClass: T.Type, in bundle: Bundle? = nil) {

        register(T.nib, forCellReuseIdentifier: T.reuseIdentifier)
    }

    public func register<T: UITableViewCell>(_ cellClass: T.Type) {

        register(cellClass, forCellReuseIdentifier: cellClass.reuseIdentifier)
    }

    public func registerHeaderFooter<T: UITableViewHeaderFooterView>(_ cellClass: T.Type) {

        register(cellClass, forHeaderFooterViewReuseIdentifier: cellClass.reuseIdentifier)
    }
    
    public func dequeueReusableCell<T: UITableViewCell>(for indexPath: IndexPath) -> T {

        return dequeueReusableCell(withIdentifier: T.reuseIdentifier, for: indexPath) as! T
    }

    public func dequeueReusableHeaderFooter<T: UITableViewHeaderFooterView>(for section: Int) -> T {
    
        return dequeueReusableHeaderFooterView(withIdentifier: T.reuseIdentifier) as! T
    }
    
    public func cell<T: UITableViewCell>(at indexPath: IndexPath) -> T {
        
        return cellForRow(at: indexPath) as! T
    }
}
