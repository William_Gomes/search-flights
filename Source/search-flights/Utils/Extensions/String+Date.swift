import Foundation

extension String {

    func convertISO8601(to dateFormat: DateFormat) -> String? {
        
        guard let date = transformToDate() else {
            return nil
        }
        
        return convertDateToString(date: date, using: dateFormat)
    }
    
    private func transformToDate() -> Date? {
        
        let isoFormatter = ISO8601DateFormatter()
        isoFormatter.formatOptions =  [.withInternetDateTime, .withFractionalSeconds]
        
        let dateToFormat = last == "Z" ? self : self + "Z"
        guard let date = isoFormatter.date(from: dateToFormat) else {
            return nil
        }
        
        return date
    }
    
    private func convertDateToString(date: Date, using dateFormat: DateFormat) -> String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = dateFormat.rawValue
        
        return dateFormatter.string(from: date)
    }
}
