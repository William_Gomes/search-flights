import UIKit

extension UIViewController {
    
    func showErrorMessage<T: Error>(with error: UIError<T>, completion: (() -> Void)? = nil) {
        
        let message: String
        
        switch error {
            
        case .internalError:
            
            message = "Internal server error"
            
        case .noInternetConnection:
            
            message = "No internet connection"
            
        case .serverError(let serverError):
            
            message = serverError.localizedDescription
        }
        
        showAlert(title: "Err", message: message, completion: completion)
    }
    
    func show(_ message: String, completion: (() -> Void)? = nil) {
        
        showAlert(title: "Warning", message: message, completion: completion)
    }
    
    private func showAlert(title: String, message: String, completion: (() -> Void)? = nil) {
        
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { _ in
            
            completion?()
        })
        
        alertController.addAction(okAction)
        present(alertController, animated: true)
    }
}
