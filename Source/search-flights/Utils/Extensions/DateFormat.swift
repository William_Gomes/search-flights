import Foundation

enum DateFormat: String {
    
    case yyyyMMdd = "yyyy-MM-dd"
    case hhMM = "HH:mm"
}
