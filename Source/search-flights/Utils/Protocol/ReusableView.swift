import UIKit

public protocol ReusableView: class {
    
    static var reuseIdentifier: String { get }
    static var nib: UINib { get }
}

extension ReusableView {
    
    public static var reuseIdentifier: String {

        return String(describing: self)
    }

    public static var nib: UINib {

        return UINib(nibName: reuseIdentifier, bundle: nil)
    }
}
