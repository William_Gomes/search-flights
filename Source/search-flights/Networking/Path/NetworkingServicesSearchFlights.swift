import Alamofire
import Foundation

enum NetworkingServicesSearchFlights: NetworkingServicesProtocol {
    
    case stations
    case searchResults(_ searchResultsRequestModel: SearchResultsRequestModel)
    
    var basePath: String {
        
        switch self {
            
        case .stations:
            
            return "https://tripstest.ryanair.com/static/"
            
        case .searchResults:
            
            return "https://sit-nativeapps.ryanair.com/"
        }
    }
    
    var path: String {
        
        switch self {
            
        case .stations:
            
            return "stations.json"
            
        case .searchResults:
            
            return "api/v4/Availability"
        }
    }
    
    var method: HTTPMethod {

        return .get
    }
    
    var encoding: ParameterEncoding {

        return URLEncoding()
    }
    
    var parameters: Parameters? {
        
        switch self {
            
        case .stations:
            
            return nil
            
        case .searchResults(let searchResultsRequestModel):
            
            return ["origin": searchResultsRequestModel.origin.code,
                    "destination": searchResultsRequestModel.destination.code,
                    "dateout": searchResultsRequestModel.formattedDateOut,
                    "flexdaysbeforeout": "3",
                    "flexdaysout": "3",
                    "flexdaysbeforein": "3",
                    "flexdaysin": "3",
                    "adt": String(searchResultsRequestModel.totalAdults),
                    "teen": String(searchResultsRequestModel.totalTeens),
                    "chd": String(searchResultsRequestModel.totalChildren),
                    "roundtrip": "false",
                    "ToUs": "AGREED"]
        }
    }
    
    var headers: HTTPHeaders? {

        return HTTPHeaders.default
    }
}
