import Alamofire
import Foundation

protocol NetworkingServicesProtocol: URLRequestConvertible {

    var basePath: String { get }
    var path: String { get }
    var method: HTTPMethod { get }
    var encoding: ParameterEncoding { get }
    var parameters: Parameters? { get }
    var headers: HTTPHeaders? { get }
}

extension NetworkingServicesProtocol {

    func asURLRequest() throws -> URLRequest {

        guard let url = URL(string: basePath + path) else {
            throw NetworkingError<String>.invalidURL
        }

        let urlRequest = try URLRequest(url: url, method: method, headers: headers)
        let encodedURLRequest = try encoding.encode(urlRequest, with: parameters)
        return encodedURLRequest
    }
}
