import Foundation

struct SearchResultsRequestModel {
    
    let origin: StationModel
    let destination: StationModel
    let dateOut: Date
    let totalAdults: Int
    let totalTeens: Int
    let totalChildren: Int
    
    var formattedDateOut: String {
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = DateFormat.yyyyMMdd.rawValue
        let dateFormatted = dateFormatter.string(from: dateOut)
        return dateFormatted
    }
}
