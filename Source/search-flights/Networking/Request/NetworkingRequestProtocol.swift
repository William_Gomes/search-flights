import Foundation

protocol NetworkingRequestProtocol {
    
    func request<P: ParserProtocol>(_ networkingServicesProtocol: NetworkingServicesProtocol,
                                    parser: P,
                                    onSuccess: @escaping (P.ModelType) -> Void,
                                    onError: @escaping (NetworkingError<P.ErrorType>) -> Void)
    
    func cancel()
}
