import Alamofire
import Foundation

final class NetworkingRequest: NetworkingRequestProtocol {
    
    private let session: Session
    private let dispatchQueueLabel = "com.Networking.requestQueue"
    
    private var dataRequest: DataRequest?
    
    init(session: Session) {

        self.session = session
    }
    
    func request<P: ParserProtocol>(_ networkingServicesProtocol: NetworkingServicesProtocol,
                                    parser: P,
                                    onSuccess: @escaping (P.ModelType) -> Void,
                                    onError: @escaping (NetworkingError<P.ErrorType>) -> Void) {
        
        dataRequest = session.request(networkingServicesProtocol)
        
        let queue = DispatchQueue(label: dispatchQueueLabel, qos: .default, attributes: .concurrent)
        
        dataRequest?.validate().responseData(queue: queue, completionHandler: { [weak self] response in
            
            guard let strongSelf = self else {
                
                onError(NetworkingError<P.ErrorType>.selfDeallocated)
                return
            }
            
            strongSelf.parseResponse(response: response,
                                     parser: parser,
                                     onSuccess: { responseModel in
                                        
                                        DispatchQueue.main.async {
                                            onSuccess(responseModel)
                                        }
                                        
            }, onError: { responseError in
                
                DispatchQueue.main.async {
                    onError(responseError)
                }
            })
        })
    }
    
    func cancel() {
        
        dataRequest?.cancel()
    }
    
    private func parseResponse<P: ParserProtocol>(response: AFDataResponse<Data>,
                                                  parser: P,
                                                  onSuccess: @escaping (P.ModelType) -> Void,
                                                  onError: @escaping (NetworkingError<P.ErrorType>) -> Void) {
        
        switch response.result {

        case .success(let data):
            
            let result = parser.parseData(data: data)
            
            switch result {
                
            case .success(let responseModel):
                
                onSuccess(responseModel)
                
            case .failure(let apiError):
                
                onError(apiError)
            }
            
        case .failure(let error):

            verifyResponseError(response: response, error: error, parser: parser, onError: onError)
        }
    }
    
    private func verifyResponseError<P: ParserProtocol>(response: AFDataResponse<Data>,
                                                        error: Error,
                                                        parser: P,
                                                        onError: @escaping (NetworkingError<P.ErrorType>) -> Void) {
        
        if let statusCode = response.response?.statusCode,
            let data = response.data {
            
            onError(parser.parseError(statusCode: statusCode, data: data))
            
        } else if let statusCode = response.response?.statusCode {
            
            guard let httpStatusCode = HTTPStatusCode(rawValue: statusCode) else {
                
                onError(.invalidHTTPStatusCode(code: statusCode))
                return
            }
            
            onError(.httpStatusCode(httpError: httpStatusCode))

        } else {
            
            onError(translateResponseError(error))
        }
    }
    
    private func translateResponseError<P: Error>(_ error: Error) -> NetworkingError<P> {
        
        let nsError = error as NSError
    
        guard let afError = error.asAFError else {

            return .notDefined(code: nsError.code, message: nsError.localizedDescription)
        }
    
        if case let AFError.sessionTaskFailed(error as NSError) = afError, error.code == -1009 {
            
            return .noInternetConnection
            
        } else if case AFError.invalidURL = afError {
            
            return .invalidURL
        }

        return .notDefined(code: nsError.code, message: nsError.localizedDescription)
    }
}

// Exposing private methods to test purpouse."
#if DEBUG
extension NetworkingRequest {
    
    func exposedParseResponse<P: ParserProtocol>(response: AFDataResponse<Data>,
                                                 parser: P,
                                                 onSuccess: @escaping (P.ModelType) -> Void,
                                                 onError: @escaping (NetworkingError<P.ErrorType>) -> Void) {
        
        return parseResponse(response: response, parser: parser, onSuccess: onSuccess, onError: onError)
    }
    
    func exposedVerifyResponseError<P: ParserProtocol>(response: AFDataResponse<Data>,
                                                       error: Error,
                                                       parser: P,
                                                       onError: @escaping (NetworkingError<P.ErrorType>) -> Void) {
        
        verifyResponseError(response: response, error: error, parser: parser, onError: onError)
    }

    func exposedTranslateResponseError<P: Error>(_ error: Error) -> NetworkingError<P> {
        
        return translateResponseError(error)
    }
}
#endif
