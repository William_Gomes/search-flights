import Foundation

final class NetworkingRequestMock: NetworkingRequestProtocol {
    
    func request<P: ParserProtocol>(_ networkingServicesProtocol: NetworkingServicesProtocol,
                                    parser: P,
                                    onSuccess: @escaping (P.ModelType) -> Void,
                                    onError: @escaping (NetworkingError<P.ErrorType>) -> Void) {
        
        if let path = Bundle.main.path(forResource: "SearchResult", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                
                let result = parser.parseData(data: data)
                
                switch result {
                    
                case .success(let responseModel):
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                        onSuccess(responseModel)
                    }
                    
                case .failure(let apiError):
                    
                    onError(apiError)
                }
            } catch {
                
                onError(NetworkingError<P.ErrorType>.notDefined(code: 1, message: "Invalid JSON"))
            }
        } else {
            onError(NetworkingError<P.ErrorType>.notDefined(code: 1, message: "Invalid JSON"))
        }
    }
    
    func cancel() {
        
    }
}
