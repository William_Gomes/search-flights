import Alamofire
import Foundation

final class NetworkingRequestFactory {

    private init() { }

    static func makeNetworkingRequest() -> NetworkingRequest {

        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 10

        let session = Session(configuration: configuration)
        return NetworkingRequest(session: session)
    }
}
