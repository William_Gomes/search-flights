import Foundation

enum NetworkingError<T: Error>: Error {

    case noInternetConnection
    case invalidURL
    case unableToParseData
    case selfDeallocated
    case httpStatusCode(httpError: HTTPStatusCode)
    case invalidHTTPStatusCode(code: Int)
    case serverError(error: T)
    case notDefined(code: Int, message: String)
}

extension NetworkingError: Equatable {
    
    static func ==<T: Error>(lhs: NetworkingError<T>, rhs: NetworkingError<T>) -> Bool {
        
        switch (lhs, rhs) {
        case (.noInternetConnection, .noInternetConnection):
            return true
        case (.invalidURL, .invalidURL):
            return true
        case (.unableToParseData, .unableToParseData):
            return true
        case (.selfDeallocated, .selfDeallocated):
            return true
        case let (.httpStatusCode(a), .httpStatusCode(b)):
            return a == b
        case let (.invalidHTTPStatusCode(a), .invalidHTTPStatusCode(b)):
            return a == b
        case let (.serverError(a), .serverError(b)):
            return a.localizedDescription == b.localizedDescription
        case let (.notDefined(a, _), .notDefined(b, _)):
            return a == b
        default:
            return false
        }
    }
}
