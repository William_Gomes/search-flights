import Foundation

struct FareType: Codable {
    
    let fares: [FareModel]
    
    enum CodingKeys: String, CodingKey {
        
        case fares = "fares"
    }
}
