import Foundation

struct FlightModel: Codable {
    
    let flightNumber: String
    let regularFare: FareType
    let timeUTC: [String]
    
    var hourBegin: String? {
        
        guard let date = timeUTC.first else {
            return nil
        }

        return date.convertISO8601(to: .hhMM)
    }

    var hourEnd: String? {

        guard let date = timeUTC.last else {
            return nil
        }
        
        return date.convertISO8601(to: .hhMM)
    }
    
    enum CodingKeys: String, CodingKey {
        
        case flightNumber = "flightNumber"
        case regularFare = "regularFare"
        case timeUTC = "timeUTC"
    }
}
