import Foundation

struct FareModel: Codable {
    
    let publishedFare: Float
    let hasDiscount: Bool
    let discountAmount: Float

    var discontedFare: Float {

        return hasDiscount ? publishedFare - discountAmount : publishedFare
    }

    enum CodingKeys: String, CodingKey {
        
        case publishedFare = "publishedFare"
        case hasDiscount = "hasDiscount"
        case discountAmount = "discountAmount"
    }
}
