import Foundation

struct StationModel: Codable {
    
    let code: String
    let name: String
    let markets: [MarketModel]
    
    var formatedName: String {

        return "\(name) - \(code)"
    }

    enum CodingKeys: String, CodingKey {
        
        case code = "code"
        case name = "name"
        case markets = "markets"
    }
}
