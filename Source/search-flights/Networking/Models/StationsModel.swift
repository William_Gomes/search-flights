import Foundation

struct StationsModel: Codable {
    
    let stations: [StationModel]
    
    enum CodingKeys: String, CodingKey {
        
        case stations = "stations"
    }
}
