import Foundation

struct SearchResultsModel: Codable {
    
    let currency: String
    let trips: [TripModel]
    
    enum CodingKeys: String, CodingKey {
        
        case currency = "currency"
        case trips = "trips"
    }
}
