import Foundation

struct DateModel: Codable {
    
    let dateOut: String
    let flights: [FlightModel]
    
    var formattedDateOut: String? {

        return dateOut.convertISO8601(to: .yyyyMMdd)
    }

    enum CodingKeys: String, CodingKey {
        
        case dateOut = "dateOut"
        case flights = "flights"
    }
}
