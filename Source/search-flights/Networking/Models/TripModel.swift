import Foundation

struct TripModel: Codable {
    
    var dates: [DateModel]
    
    enum CodingKeys: String, CodingKey {
        
        case dates = "dates"
    }
}
