import Foundation

struct MarketModel: Codable {
    
    let code: String
    
    enum CodingKeys: String, CodingKey {
        
        case code = "code"
    }
}
