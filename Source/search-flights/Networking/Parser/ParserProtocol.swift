import Foundation

protocol ParserProtocol {

    associatedtype ModelType: Codable
    associatedtype ErrorType: Error
    
    func parseData(data: Data) -> Result<ModelType, NetworkingError<ErrorType>>
    func parseError(statusCode: Int, data: Data) -> NetworkingError<ErrorType>
}
