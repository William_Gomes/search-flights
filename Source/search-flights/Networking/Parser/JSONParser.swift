import Foundation

final class JSONParser<M: Codable>: ParserProtocol {
    
    func parseData(data: Data) -> Result<M?, NetworkingError<String>> {

        do {

            if data.isEmpty {

                return .success(nil)
            }

            let decoder = JSONDecoder()
            let model = try decoder.decode(M.self, from: data)

            return .success(model)

        } catch {

            return .failure(NetworkingError<ErrorType>.unableToParseData)
        }
    }

    func parseError(statusCode: Int, data: Data) -> NetworkingError<String> {

        guard let httpStatusCode = HTTPStatusCode(rawValue: statusCode) else {

            return NetworkingError.invalidHTTPStatusCode(code: statusCode)
        }

        return NetworkingError.httpStatusCode(httpError: httpStatusCode)
    }
}
