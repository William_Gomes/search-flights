import UIKit

final class SearchResultsViewController: BaseNibViewController {
    
    @IBOutlet private weak var tableview: UITableView!
    
    private var loadingView: LoadingView?
    
    private let presenter: SearchResultsPresenterProtocol
    private let coordinator: SearchResultsCoordinator
    
    init(presenter: SearchResultsPresenterProtocol, coordinator: SearchResultsCoordinator) {
        
        self.presenter = presenter
        self.coordinator = coordinator
        
        super.init()
    }
    
    override func viewDidLoad() {

        super.viewDidLoad()
        
        tableview.registerNib(for: SearchResultsTableViewCell.self)
        tableview.registerNib(for: NoFlightsAvailableTableViewCell.self)

        tableview.dataSource = self
        tableview.delegate = self
        presenter.fetchResults()
    }
}

extension SearchResultsViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        
        return presenter.dates.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        let totalFlights = presenter.dates[section].flights.isEmpty ? 1 : presenter.dates[section].flights.count
        return totalFlights
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let currency = presenter.currency else {
            
            return UITableViewCell()
        }

        if presenter.dates[indexPath.section].flights.isEmpty {
            
            return tableView.dequeueReusableCell(for: indexPath) as NoFlightsAvailableTableViewCell
            
        } else {
            
            let date = presenter.dates[indexPath.section].flights[indexPath.row]
            let cell = tableView.dequeueReusableCell(for: indexPath) as SearchResultsTableViewCell
            cell.set(with: date, currency: currency)
            return cell
        }
    }
}

extension SearchResultsViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return presenter.dates[section].formattedDateOut
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return SearchResultsTableViewCell.cellHeight
    }
}

extension SearchResultsViewController: SearchResultsPresenterDelegate {
    
    func showLoading() {

        loadingView = LoadingView.fromNib()
        view.addSubview(loadingView!)
    }
    
    func hideLoading() {
    
        loadingView?.removeFromSuperview()
    }
    
    func didFetchResult() {

        title = presenter.title
        tableview.reloadData()
    }
    
    func didFailFetchResult(error: UIError<String>) {
        
        showErrorMessage(with: error, completion: { [weak self] in
            
            self?.coordinator.dismiss()
        })
    }
}
