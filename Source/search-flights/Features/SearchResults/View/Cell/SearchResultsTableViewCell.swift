import UIKit

final class SearchResultsTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var hourBeginLabel: UILabel!
    @IBOutlet private weak var hourEndLabel: UILabel!
    @IBOutlet private weak var flightNumberLabel: UILabel!
    @IBOutlet private weak var fareLabel: UILabel!
    
    static let cellHeight: CGFloat = 90.0

    func set(with flightModel: FlightModel, currency: String) {
        
        hourBeginLabel.text = flightModel.hourBegin
        hourEndLabel.text = flightModel.hourEnd
        flightNumberLabel.text = flightModel.flightNumber
        
        let totalFare = flightModel.regularFare.fares.map({ $0.discontedFare }).reduce(0, +)
        fareLabel.text = "\(totalFare) \(currency)"
    }
}
