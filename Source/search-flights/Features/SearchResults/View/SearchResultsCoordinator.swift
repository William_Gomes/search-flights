import UIKit

final class SearchResultsCoordinator: Coordinator {
    
    private let navigationController: UINavigationController?
    private let searchResultsRequestModel: SearchResultsRequestModel

    init(navigationController: UINavigationController?, searchResultsRequestModel: SearchResultsRequestModel) {
        
        self.navigationController = navigationController
        self.searchResultsRequestModel = searchResultsRequestModel
    }
    
    func start() {

        let presenter = SearchResultsPresenter(searchResultsRequestModel: searchResultsRequestModel)
        let searchResultsViewController = SearchResultsViewController(presenter: presenter, coordinator: self)
        presenter.delegate = searchResultsViewController
        navigationController?.pushViewController(searchResultsViewController, animated: true)
    }
    
    func dismiss() {

        navigationController?.popViewController(animated: true)
    }
}
