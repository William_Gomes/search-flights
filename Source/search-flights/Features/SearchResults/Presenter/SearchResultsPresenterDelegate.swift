import Foundation

protocol SearchResultsPresenterDelegate: class, Loadable {
    
    func didFetchResult()
    func didFailFetchResult(error: UIError<String>)
}
