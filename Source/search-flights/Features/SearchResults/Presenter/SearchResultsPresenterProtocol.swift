import Foundation

protocol SearchResultsPresenterProtocol {
    
    var currency: String? { get }
    var dates: [DateModel] { get }
    var title: String { get }

    func fetchResults()
}
