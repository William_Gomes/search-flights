import Foundation

final class SearchResultsPresenter: SearchResultsPresenterProtocol {
    
    weak var delegate: SearchResultsPresenterDelegate?
    
    private let networkingRequest: NetworkingRequestProtocol
    private let searchResultsRequestModel: SearchResultsRequestModel

    var title: String {

        return "\(searchResultsRequestModel.origin.name) -> \(searchResultsRequestModel.destination.name)"
    }

    private(set) var currency: String?
    private(set) var dates: [DateModel] = []

    init(networkingRequest: NetworkingRequestProtocol = NetworkingRequestFactory.makeNetworkingRequest(),
         searchResultsRequestModel: SearchResultsRequestModel) {
        
        self.networkingRequest = networkingRequest
        self.searchResultsRequestModel = searchResultsRequestModel
    }
    
    func fetchResults() {
        
        delegate?.showLoading()

        let searchResults = NetworkingServicesSearchFlights.searchResults(searchResultsRequestModel)
        networkingRequest.request(searchResults,
                                  parser: JSONParser<SearchResultsModel>(),
                                  onSuccess: { [weak self] resposneModel in
                                    
                                    self?.delegate?.hideLoading()

                                    guard let dates = resposneModel?.trips.first?.dates else {

                                        return
                                    }
                                    
                                    self?.currency = resposneModel?.currency
                                    self?.dates = dates
                                    self?.delegate?.didFetchResult()

            }, onError: { [weak self] error in
                
                let uiError = UIError<String>.translate(with: error)
                self?.delegate?.hideLoading()
                self?.delegate?.didFailFetchResult(error: uiError)
        })
        
    }
}
