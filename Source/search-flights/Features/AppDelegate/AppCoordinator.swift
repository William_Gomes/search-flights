import UIKit

final class AppCoordinator: Coordinator {

    private weak var window: UIWindow?
    
    private let navigationController = UINavigationController()
    private let searchFlightsCoordinator: SearchFlightsCoordinator

    init(window: UIWindow) {

        self.window = window
        searchFlightsCoordinator = SearchFlightsCoordinator(navigationController: navigationController)
    }

    func start() {

        window?.rootViewController = navigationController
        searchFlightsCoordinator.start()
        window?.makeKeyAndVisible()
    }
}
