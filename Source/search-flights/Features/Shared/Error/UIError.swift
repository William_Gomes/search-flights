import Foundation

enum UIError<T: Error>: Error, Equatable {
   
    case internalError
    case noInternetConnection
    case serverError(serverError: T)

    static func translate<T: Error>(with error: NetworkingError<T>) -> UIError<T> {
        
        switch error {
            
        case .httpStatusCode:
            
            return .internalError
            
        case .invalidHTTPStatusCode:
            
            return .internalError
            
        case .invalidURL, .selfDeallocated, .unableToParseData:
            
            return .internalError
            
        case .noInternetConnection:
            
            return .noInternetConnection
            
        case .notDefined:
            
            return .internalError
            
        case .serverError(let serverError):
            
            return .serverError(serverError: serverError)
        }
    }
    
    static func == (lhs: UIError<T>, rhs: UIError<T>) -> Bool {
        
        switch (lhs, rhs) {
        case (.noInternetConnection, .noInternetConnection):
            return true
        case (.internalError, .internalError):
            return true
        case let (.serverError(a), .serverError(b)):
            return a.localizedDescription == b.localizedDescription
        default:
            return false
        }
    }
}
