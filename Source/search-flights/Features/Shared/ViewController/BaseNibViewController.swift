import UIKit

class BaseNibViewController: UIViewController {

    internal init() {
        
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }
    
    @available(*, unavailable)
    required init?(coder aDecoder: NSCoder) {
        
        super.init(coder: aDecoder)
    }
}
