import Foundation

protocol Loadable {
    
    func showLoading()
    func hideLoading()
}
