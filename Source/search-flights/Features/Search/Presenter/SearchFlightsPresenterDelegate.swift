import Foundation

protocol SearchFlightsPresenterDelegate: class {
    
    func didChange(originStation: StationModel?)
    func didChange(destinationStation: StationModel?)
}
