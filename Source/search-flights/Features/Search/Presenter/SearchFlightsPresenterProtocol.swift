import Foundation

protocol SearchFlightsPresenterProtocol {
    
    var delegate: SearchFlightsPresenterDelegate? { get set }
    var stations: [StationModel] { get }
    var originStation: StationModel? { get set }
    var destinationStation: StationModel? { get set }
    var minimumAdults: Int { get }
    var minimumTeens: Int { get }
    var minimumChildren: Int { get }
    var minimumDate: Date { get }
    var selectedDate: Date { get set }
    var formattedSelectedDate: String { get }

    func set(station: StationModel, for stationType: StationType)
}
