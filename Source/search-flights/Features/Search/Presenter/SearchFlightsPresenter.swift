import Foundation

final class SearchFlightsPresenter: SearchFlightsPresenterProtocol {

    private(set) var minimumAdults = 1
    private(set) var minimumTeens = 0
    private(set) var minimumChildren = 0
    private(set) var stations: [StationModel] = []

    private(set) var minimumDate = Date()
    var selectedDate = Date()

    var formattedSelectedDate: String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = .medium
        let dateFormatted = dateFormatter.string(from: selectedDate)
        return dateFormatted
    }

    var originStation: StationModel? {
        didSet {
            delegate?.didChange(originStation: originStation)
            destinationStation = nil
            delegate?.didChange(destinationStation: destinationStation)
        }
    }

    var destinationStation: StationModel? {
        didSet {
            delegate?.didChange(destinationStation: destinationStation)
        }
    }

    weak var delegate: SearchFlightsPresenterDelegate?

    func set(station: StationModel, for stationType: StationType) {

        switch stationType {
            
        case .origin:
            
            originStation = station
            
        case .destination:
            
            destinationStation = station
        }
    }
}
