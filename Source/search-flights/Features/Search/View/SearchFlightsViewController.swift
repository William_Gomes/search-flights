import UIKit

final class SearchFlightsViewController: BaseNibViewController {
    
    @IBOutlet private weak var flightOriginButton: UIButton!
    @IBOutlet private weak var flightDestinationButton: UIButton!
    @IBOutlet private weak var flyOutTextField: UITextField!
    @IBOutlet private weak var totalAdultsTextField: UITextField!
    @IBOutlet private weak var totalTeensTextField: UITextField!
    @IBOutlet private weak var totalChildrenTextField: UITextField!

    private var presenter: SearchFlightsPresenterProtocol
    private var totalAdultsPicker: PeopleTotalPicker?
    private var totalTeensPicker: PeopleTotalPicker?
    private var totalChildrenPicker: PeopleTotalPicker?

    private let placeholder = "Select"

    init(presenter: SearchFlightsPresenterProtocol = SearchFlightsPresenter()) {

        self.presenter = presenter

        super.init()
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        setupView()
    }
    
    private func setupView() {

        title = "Search Flights"

        flightOriginButton.setTitle(placeholder, for: .normal)
        flightDestinationButton.setTitle(placeholder, for: .normal)
        flyOutTextField.text = presenter.formattedSelectedDate
        setupDatePicker()
        setupTotalPeoplePickers()
    }

    private func setupTotalPeoplePickers() {

        totalAdultsPicker = PeopleTotalPicker(minValue: presenter.minimumAdults, textField: totalAdultsTextField)
        totalTeensPicker = PeopleTotalPicker(minValue: presenter.minimumTeens, textField: totalTeensTextField)
        totalChildrenPicker = PeopleTotalPicker(minValue: presenter.minimumChildren, textField: totalChildrenTextField)
    }

    private func setupDatePicker() {

        let datePicker = UIDatePicker()
        datePicker.minimumDate = presenter.minimumDate
        datePicker.date = presenter.selectedDate
        datePicker.datePickerMode = .date
        datePicker.addTarget(self, action: #selector(datePickerChanged(picker:)), for: .valueChanged)
        flyOutTextField.inputView = datePicker
        flyOutTextField.placeholder = placeholder
    }

    @objc
    private func datePickerChanged(picker: UIDatePicker) {

        presenter.selectedDate = picker.date
        flyOutTextField.text = presenter.formattedSelectedDate
    }

    @IBAction private func didTapFlightFromButton(_ sender: Any) {
        
        view.endEditing(true)
        let coordinator = StationSelectionCoordinator(navigationController: navigationController,
                                                      stationType: .origin)
        coordinator.delegate = self
        coordinator.start()
    }
    
    @IBAction private func didTapFlightToButton(_ sender: Any) {

        guard let originStation = presenter.originStation else {

            return
        }
        
        view.endEditing(true)
        let coordinator = StationSelectionCoordinator(navigationController: navigationController,
                                                      stationType: .destination(originStation: originStation))
        coordinator.delegate = self
        coordinator.start()
    }
    
    @IBAction private func didTapSearchButton(_ sender: Any) {
        
        guard let origin = presenter.originStation,
            let destination = presenter.destinationStation,
            let totalAdults = totalAdultsPicker?.selectedValue,
            let totalTeens = totalTeensPicker?.selectedValue,
            let totalChildren = totalChildrenPicker?.selectedValue else {

                return
        }
        
        let searchResultsRequestModel = SearchResultsRequestModel(origin: origin,
                                                                  destination: destination,
                                                                  dateOut: presenter.selectedDate,
                                                                  totalAdults: totalAdults,
                                                                  totalTeens: totalTeens,
                                                                  totalChildren: totalChildren)

        let coordinator = SearchResultsCoordinator(navigationController: navigationController,
                                                   searchResultsRequestModel: searchResultsRequestModel)
        coordinator.start()
    }
}

extension SearchFlightsViewController: StationSelectionViewDelegate {
    
    func didSelect(station: StationModel, stationType: StationType) {

        presenter.set(station: station, for: stationType)
    }
}

extension SearchFlightsViewController: SearchFlightsPresenterDelegate {

    func didChange(originStation: StationModel?) {

        flightOriginButton.setTitle(originStation?.formatedName ?? placeholder, for: .normal)
    }
    
    func didChange(destinationStation: StationModel?) {

        flightDestinationButton.setTitle(destinationStation?.formatedName ?? placeholder, for: .normal)
    }
}
