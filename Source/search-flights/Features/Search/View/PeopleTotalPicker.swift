import UIKit

final class PeopleTotalPicker: NSObject {
    
    private let picker = UIPickerView()
    private let peopleAmount: [Int]
    private let minValue: Int

    private var textField: UITextField?

    private(set) var selectedValue: Int

    init(minValue: Int, textField: UITextField) {
        self.minValue = minValue
        selectedValue = minValue

        self.textField = textField
        self.textField?.inputView = picker

        peopleAmount = Array(minValue...6)
    
        super.init()

        textField.text = String(minValue)
        picker.delegate = self
        picker.dataSource = self
    }
}

extension PeopleTotalPicker: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return String(peopleAmount[row])
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        selectedValue = peopleAmount[row]
        textField?.text = String(selectedValue)
    }
}

extension PeopleTotalPicker: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {

        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {

        return peopleAmount.count
    }
}
