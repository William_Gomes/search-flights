import UIKit

final class SearchFlightsCoordinator: Coordinator {
    
    private let navigationController: UINavigationController?

    init(navigationController: UINavigationController?) {

        self.navigationController = navigationController
    }

    func start() {

        let presenter = SearchFlightsPresenter()
        let searchFlightsViewController = SearchFlightsViewController(presenter: presenter)
        presenter.delegate = searchFlightsViewController
        navigationController?.pushViewController(searchFlightsViewController, animated: false)
    }
}
