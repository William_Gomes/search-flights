import Foundation

protocol StationSelectionPresenterProtocol {
    
    var stationType: StationType { get }
    var currentStations: [StationModel] { get }
    var title: String { get }

    func fetchStations()
    func filterStations(by query: String) 
    func clearFilter()
}
