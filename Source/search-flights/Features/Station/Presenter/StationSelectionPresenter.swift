import Foundation

final class StationSelectionPresenter: StationSelectionPresenterProtocol {
    
    private let networkingRequest: NetworkingRequestProtocol
    
    private(set) var stationType: StationType
    
    private static var allStations: [StationModel]?
    
    private var stations: [StationModel]?
    private var searchedStations: [StationModel]?
    
    var currentStations: [StationModel] {
        
        guard let searchedStations = searchedStations else {
            
            return stations ?? []
        }
        
        return searchedStations
    }
    
    var title: String {
        
        switch stationType {
            
        case .origin:
            
            return "From"
            
        case .destination:
            
            return "To"
        }
    }
    
    weak var delegate: StationSelectionPresenterDelegate?
    
    init(networkingRequest: NetworkingRequestProtocol = NetworkingRequestFactory.makeNetworkingRequest(),
         stationType: StationType) {
        
        self.networkingRequest = networkingRequest
        self.stationType = stationType
    }
    
    func fetchStations() {
        
        if StationSelectionPresenter.allStations != nil {
            
            filterStations(by: stationType)
            return
        }
        
        delegate?.showLoading()
        networkingRequest.request(NetworkingServicesSearchFlights.stations,
                                  parser: JSONParser<StationsModel>(),
                                  onSuccess: { [weak self] responseModel in
                                    
                                    guard let self = self else {
                                        
                                        return
                                    }
                                    
                                    StationSelectionPresenter.allStations = responseModel?.stations ?? []
                                    self.delegate?.hideLoading()
                                    self.filterStations(by: self.stationType)
                                    
            }, onError: { [weak self] networkingError in
                
                self?.delegate?.hideLoading()
                let error = UIError<String>.translate(with: networkingError)
                self?.delegate?.display(error: error)
        })
    }
    
    func filterStations(by query: String) {
        
        guard !query.isEmpty else {
            
            clearFilter()
            return
        }
        
        let searchTerm = query.lowercased()
        searchedStations = stations?.filter({ station in
            
            return station.code.lowercased().contains(searchTerm) || station.name.lowercased().contains(searchTerm)
        })
        
        delegate?.didFetchStations()
    }
    
    func clearFilter() {
        
        searchedStations = nil
        delegate?.didFetchStations()
    }
    
    private func filterStations(by stationType: StationType) {
        
        switch stationType {
            
        case .origin:
            
            stations = StationSelectionPresenter.allStations
            
        case let .destination(originStation):
            
            let filteredStations = StationSelectionPresenter.allStations?.filter({ station in
                
                return originStation.markets.contains(where: { market in
                    
                    return market.code == station.code
                })
            })
            
            stations = filteredStations
        }
        
        if currentStations.isEmpty {
            
            delegate?.showEmptyResults()
            
        } else {
            
            delegate?.didFetchStations()
        }
    }
}
