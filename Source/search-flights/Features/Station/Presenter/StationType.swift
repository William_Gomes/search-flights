import Foundation

enum StationType {
    
    case origin
    case destination(originStation: StationModel)
}
