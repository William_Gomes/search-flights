import Foundation

protocol StationSelectionPresenterDelegate: class, Loadable {
    
    func didFetchStations()
    func showEmptyResults()
    func display(error: UIError<String>)
}
