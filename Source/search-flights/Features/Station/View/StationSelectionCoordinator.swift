import UIKit

final class StationSelectionCoordinator: Coordinator {
    
    private let navigationController: UINavigationController?
    private let stationType: StationType
    
    weak var delegate: StationSelectionViewDelegate?
    
    init(navigationController: UINavigationController?, stationType: StationType) {
        
        self.navigationController = navigationController
        self.stationType = stationType
    }
    
    func start() {
        
        let presenter = StationSelectionPresenter(stationType: stationType)
        let stationSelectionViewController = StationSelectionViewController(presenter: presenter,
                                                                            coordinator: self)
        presenter.delegate = stationSelectionViewController
        stationSelectionViewController.delegate = delegate
        navigationController?.pushViewController(stationSelectionViewController, animated: true)
    }
    
    func dismiss() {

        navigationController?.popViewController(animated: true)
    }
}
