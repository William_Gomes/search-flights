import UIKit

final class StationSelectionTableViewCell: UITableViewCell {
    
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private weak var codeLabel: UILabel!
    
    static let cellHeight: CGFloat = 75.0
    
    func set(with stationModel: StationModel) {
        
        nameLabel.text = stationModel.name
        codeLabel.text = stationModel.code
    }
}
