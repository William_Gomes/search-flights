import UIKit

final class StationSelectionViewController: BaseNibViewController {
    
    @IBOutlet private weak var tableView: UITableView!
    
    private let presenter: StationSelectionPresenterProtocol
    private let coordinator: StationSelectionCoordinator
    private let searchController = UISearchController(searchResultsController: nil)
    
    private var loadingView: LoadingView?
    
    weak var delegate: StationSelectionViewDelegate?
    
    init(presenter: StationSelectionPresenterProtocol, coordinator: StationSelectionCoordinator) {
        
        self.presenter = presenter
        self.coordinator = coordinator
        
        super.init()
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()

        setupView()
        presenter.fetchStations()
    }
    
    private func setupView() {
        
        title = presenter.title

        tableView.registerNib(for: StationSelectionTableViewCell.self)
        tableView.dataSource = self
        tableView.delegate = self
        setupSearchController()
    }
    
    private func setupSearchController() {

        searchController.searchResultsUpdater = self
        searchController.obscuresBackgroundDuringPresentation = false
        searchController.searchBar.placeholder = "Search"
        searchController.searchBar.delegate = self
        navigationItem.searchController = searchController
        definesPresentationContext = true
    }
}

extension StationSelectionViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return presenter.currentStations.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(for: indexPath) as StationSelectionTableViewCell
        cell.set(with: presenter.currentStations[indexPath.row])
        return cell
    }
}

extension StationSelectionViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        return StationSelectionTableViewCell.cellHeight
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let station = presenter.currentStations[indexPath.row]
        delegate?.didSelect(station: station, stationType: presenter.stationType)
        coordinator.dismiss()
    }
}

extension StationSelectionViewController: UISearchResultsUpdating {
    
    func updateSearchResults(for searchController: UISearchController) {
        
        presenter.filterStations(by: searchController.searchBar.text ?? "")
    }
}

extension StationSelectionViewController: UISearchBarDelegate {
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        
        presenter.clearFilter()
    }
}

extension StationSelectionViewController: StationSelectionPresenterDelegate {
    
    func didFetchStations() {

        tableView.reloadData()
    }
    
    func showEmptyResults() {
        
        show("No Results found", completion: { [weak self] in
            
            self?.coordinator.dismiss()
        })
    }
    
    func display(error: UIError<String>) {
        
        showErrorMessage(with: error, completion: { [weak self] in

            self?.coordinator.dismiss()
        })
    }
    
    func showLoading() {
        
        loadingView = LoadingView.fromNib()
        view.addSubview(loadingView!)
    }
    
    func hideLoading() {
        
        loadingView?.removeFromSuperview()
    }
}
