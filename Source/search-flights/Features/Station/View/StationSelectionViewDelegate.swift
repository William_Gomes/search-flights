import Foundation

protocol StationSelectionViewDelegate: class {
    
    func didSelect(station: StationModel, stationType: StationType)
}
